# Raffle

Raffle is a crius plugin that allows viewers to enter and then have one person randomly selected.

## Commands

### Start Raffle
> `startraffle`

Start a raffle. Usage: `startraffle [name]`

### Enter Raffle
> `enterraffle`

Enter a raffle (you can only enter once). Usage: `enterraffle [name]`

### Draw Raffle
> `drawraffle`

Draw a raffle. Usage: `drawraffle [name]`
